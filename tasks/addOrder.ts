import { task } from "hardhat/config";


task("addorder", "Add new order in TRADE round")
    .addParam("amounteth", "amount of ETH")
    .addParam("amountkzt", "amount of tokens")
    .addParam("contract", "address of platform")
    .setAction(async (taskArgs, hre) => {
        const contract = await hre.ethers.getContractAt("ACDMPlatform", taskArgs.contract);
        await contract.addOrder(
            taskArgs.amountkzt,
            taskArgs.amounteth
        );
    });