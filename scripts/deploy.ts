import { ethers } from "hardhat";

const ROUND_TIME = 3; // in days
const TOKEN = "";
async function main() {
  const Platform = await ethers.getContractFactory("ACDMPlatform");
  const platform = await Platform.deploy(
    "0xFA5BB4d3E282d64a642FE9F1d72010A7cE091Fe2",
    3
  );

  await platform.deployed();

  console.log("platform deployed to:", platform.address);
}

main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
