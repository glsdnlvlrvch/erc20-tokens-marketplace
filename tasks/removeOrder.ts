import { task } from "hardhat/config";


task("removeorder", "Delete order in TRADE round")
    .addParam("id", "order id")
    .addParam("contract", "address of platform")
    .setAction(async (taskArgs, hre) => {
        const contract = await hre.ethers.getContractAt("ACDMPlatform", taskArgs.contract);
        await contract.removeOrder(
            taskArgs.id,
        );
    });