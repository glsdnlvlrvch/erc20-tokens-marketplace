import { expect } from "chai";
import { Signer, Contract, BigNumber } from "ethers";
import { ethers } from "hardhat";

describe("ACDM Platform", function () {
    let signers: Signer[];
    let contract: Contract;
    let token: Contract;

    const ROUND_TIME = 3; // in days
    const NAME = "KZT Token";
    const SYMBOL = "KZT";

    let user_1: Signer;
    let user_2: Signer;
    let user_3: Signer;
    let user_4: Signer;
    let user_5: Signer;
    let user_6: Signer;

    beforeEach(async function () {
        signers = await ethers.getSigners();

        // authorized
        user_1 = signers[1];
        user_2 = signers[2];
        user_3 = signers[3];
        user_4 = signers[4];

        // not authorized
        user_5 = signers[5];
        user_6 = signers[6];

        let TokenFactory = await ethers.getContractFactory("Token");
        token = await TokenFactory.deploy(
            NAME,
            SYMBOL,
        );
        await token.deployed();
        
        let ACDMFactory = await ethers.getContractFactory("ACDMPlatform");
        contract = await ACDMFactory.deploy(
            token.address,
            ROUND_TIME
        );
        await contract.deployed();

        await contract.connect(user_1).register(
            await signers[0].getAddress()
        );

        await contract.connect(user_2).register(
            await user_1.getAddress()
        );

        await contract.connect(user_3).register(
            await user_2.getAddress()
        );

        await contract.connect(user_4).register(
            await user_3.getAddress()
        );

        await contract.connect(user_5).register(
            await user_4.getAddress()
        );

        await contract.connect(user_6).register(
            await user_5.getAddress()
        );
    });

    it("Registration", async () => {
        let user_1 = signers[1];
        let user_2 = signers[2];

        await contract.connect(user_1).register(
            await signers[0].getAddress()
        );

        await contract.connect(user_2).register(
            await user_1.getAddress()
        );
        
        const User2 = await contract.users(
            await user_2.getAddress()
        );

        expect(
            User2[0]
        ).to.be.eq(
            await user_1.getAddress()
        );

        expect(
            User2[1]
        ).to.be.eq(
            await signers[0].getAddress(),
        );
    });

    it("try to call TradeOnly functions", async () => {
        await expect(
            contract.addOrder(
                1,
                1
            )
        ).to.be.revertedWith("no permission in current round");

        await expect(
            contract.removeOrder(
                0
            )
        ).to.be.revertedWith("no permission in current round");

        await expect(
            contract.redeemOrder(
                1,
                0
            )
        ).to.be.revertedWith("no permission in current round");

        await expect(
            contract.startSaleRound()
        ).to.be.revertedWith("no permission in current round");
    });

    it("try to call SaleOnly functions", async () => {
        await contract.connect(user_1).buyACDM(100000, {
            value: ethers.utils.parseEther("1.0")
        });

        await ethers.provider.send("evm_increaseTime", [300000])
        await contract.startTradeRound();

        await expect(
            contract.startTradeRound()
        ).to.be.revertedWith("no permission in current round");

        await expect(
            contract.buyACDM(1)
        ).to.be.revertedWith("no permission in current round");
    });

    describe("Sale round", function () {
        it("buyACDM function", async () => {
            await expect(
                contract.connect(user_1).buyACDM(1)
            ).to.be.revertedWith("invalid value");

            await contract.connect(user_1).buyACDM(100000, {
                value: ethers.utils.parseEther("1.0")
            });

            expect(
                await token.balanceOf(
                    await user_1.getAddress()
                )
            ).to.be.eq(100000);

            expect(
                await token.balanceOf(
                    contract.address,
                )
            ).to.be.eq(0);

            await expect(
                contract.connect(user_2).buyACDM(1, {
                    value: ethers.utils.parseEther("0.00001")
                })
            ).to.be.revertedWith("not enough balance");
        });
    });

    describe("Trade round", function () {
        beforeEach(async () => {
            await contract.connect(user_1).buyACDM(25000, {
                value: ethers.utils.parseEther("0.25")
            });
            await contract.connect(user_2).buyACDM(25000, {
                value: ethers.utils.parseEther("0.25")
            });
            await contract.connect(user_3).buyACDM(25000, {
                value: ethers.utils.parseEther("0.25")
            });
            await contract.connect(user_4).buyACDM(25000, {
                value: ethers.utils.parseEther("0.25")
            });

            await token.connect(user_1).approve(
                contract.address,
                25000
            );
            await token.connect(user_2).approve(
                contract.address,
                25000
            );
            await token.connect(user_3).approve(
                contract.address,
                25000
            );
            await token.connect(user_4).approve(
                contract.address,
                25000
            );
        })
        it("addOrder && removeOrder", async () => {
            await ethers.provider.send("evm_increaseTime", [300000])
            await contract.startTradeRound();

            await contract.connect(user_1).addOrder(
                24000,
                ethers.utils.parseEther("0.27")
            )
            
            let actual_order = (await contract.orders(0)).slice(0, 3);
            
            expect(
                actual_order[0]
            ).to.be.eq(BigNumber.from(24000));

            expect(
                actual_order[1]
            ).to.be.eq(ethers.utils.parseEther("0.27"));

            expect(
                actual_order[2]
            ).to.be.eq(
                await user_1.getAddress()
            );

            await expect(
                contract.connect(user_2).removeOrder(0)
            ).to.be.revertedWith("you have no rights");

            await contract.connect(user_1).removeOrder(0);

            actual_order = (await contract.orders(0)).slice(0, 3);

            expect(
                actual_order[0]
            ).to.be.eq(BigNumber.from(0));

            expect(
                actual_order[1]
            ).to.be.eq(ethers.utils.parseEther("0"));

            expect(
                actual_order[2]
            ).to.be.eq(
                "0x0000000000000000000000000000000000000000"
            );
        });

        it("redeemOrder && withdraw", async () => {
            await ethers.provider.send("evm_increaseTime", [300000])
            await contract.startTradeRound();
            const ethAmount1 = ethers.utils.parseEther("0.27");

            await contract.connect(user_1).addOrder(
                24000,
                ethAmount1
            );

            await expect(
                contract.redeemOrder(30000, 0)
            ).to.be.revertedWith("too much ACDM");
            await expect(
                contract.redeemOrder(24000, 0)
            ).to.be.revertedWith("invalid ETH amount");
            await expect(
                contract.redeemOrder(24000, 7)
            ).to.be.revertedWith("invalid order id")

            let beforeBalance = await user_6.getBalance();

            let sentTx = await contract.connect(user_6).redeemOrder(
                24000,
                0,
                {
                    value: ethAmount1
                }
            )
            let minedTx = await sentTx.wait();
            let fee = minedTx.effectiveGasPrice.mul(minedTx.cumulativeGasUsed);

            let afterBalance = await user_6.getBalance();

            expect(
                afterBalance
            ).to.be.eq(beforeBalance.sub(ethAmount1).sub(fee));

            expect(
                await token.balanceOf(
                    await user_6.getAddress()
                )
            ).to.be.eq(24000);

            const beforeBalance1 = await user_6.getBalance();   // recipient
            const beforeBalance2 = await user_5.getBalance();   // refer_1
            const beforeBalance3 = await user_4.getBalance();   // refer_2

            sentTx = await contract.connect(user_6).withdraw();
            minedTx = await sentTx.wait();
            fee = minedTx.effectiveGasPrice.mul(minedTx.cumulativeGasUsed);

            const afterBalance1 = await user_6.getBalance();    // recipient
            const afterBalance2 = await user_5.getBalance();    // refer_1
            const afterBalance3 = await user_4.getBalance();    // refer_2

            expect(
                afterBalance1
            ).to.be.above(beforeBalance1);

            expect(
                afterBalance1.sub(beforeBalance1).add(fee)
            ).to.be.eq(ethers.utils.parseEther("0.2565"))   // 0.27 ETH * 0.95 = 0.2565 ETH (fee for calling withdraw)
            expect(
                afterBalance2.sub(beforeBalance2)
            ).to.be.eq(ethers.utils.parseEther("0.00675")) // 0.27 ETH * 0.025 = 0.00675 ETH
            expect(
                afterBalance3.sub(beforeBalance3)
            ).to.be.eq(ethers.utils.parseEther("0.00675")) // 0.27 ETH * 0.025 = 0.00675 ETH
        });
    });

    describe("real case emulation", function () {
        it("price check", async () => {
            expect(
                await contract.price()
            ).to.be.eq(ethers.utils.parseEther("0.00001"));

            await ethers.provider.send("evm_increaseTime", [300000])
            await contract.startTradeRound();

            await ethers.provider.send("evm_increaseTime", [300000])
            await contract.startSaleRound();

            expect(
                await contract.price()
            ).to.be.eq(ethers.utils.parseEther("0.0000143"));

            await ethers.provider.send("evm_increaseTime", [300000])
            await contract.startTradeRound();

            await ethers.provider.send("evm_increaseTime", [300000])
            await contract.startSaleRound();
            
            // 0.0000143 ETH * 1,03 + 0.000004 ETH = 0.000018729 ETH
            expect(
                await contract.price()
            ).to.be.eq(ethers.utils.parseEther("0.000018729"));
            
            await ethers.provider.send("evm_increaseTime", [300000])
            await contract.startTradeRound();

            await ethers.provider.send("evm_increaseTime", [300000])
            await contract.startSaleRound();

            expect(
                await contract.price()
            ).to.be.eq(ethers.utils.parseEther("0.00002329087"));

            await ethers.provider.send("evm_increaseTime", [300000])
            await contract.startTradeRound();

            await ethers.provider.send("evm_increaseTime", [300000])
            await contract.startSaleRound();

            expect(
                await contract.price()
            ).to.be.eq(ethers.utils.parseEther("0.0000279895961"));

            await ethers.provider.send("evm_increaseTime", [300000])
            await contract.startTradeRound();

            await ethers.provider.send("evm_increaseTime", [300000])
            await contract.startSaleRound();

            expect(
                await contract.price()
            ).to.be.eq(ethers.utils.parseEther("0.000032829283983"));
        });

        it("2 SALE and TRADE rounds emulation", async () => {
            await contract.connect(user_1).buyACDM(50000, {
                value: ethers.utils.parseEther("0.5")
            });
            await contract.connect(user_2).buyACDM(40000, {
                value: ethers.utils.parseEther("0.4")
            });

            await token.connect(user_1).approve(
                contract.address,
                25000
            );
            await token.connect(user_2).approve(
                contract.address,
                25000
            );
            
            await ethers.provider.send("evm_increaseTime", [300000])
            await contract.startTradeRound();

            await contract.connect(user_1).addOrder(
                20000,
                ethers.utils.parseEther("0.3")
            );

            await contract.connect(user_2).redeemOrder(
                10000,
                0,
                {
                    value: ethers.utils.parseEther("0.15")
                }
            );

            let order = await contract.orders(0)
            expect(
                order[0]
            ).to.be.eq(10000);
            

            await ethers.provider.send("evm_increaseTime", [300000])
            await contract.startSaleRound();
            
            // get back tokens
            expect(
                await token.balanceOf(
                    await user_1.getAddress()
                )
            ).to.be.eq(40000);  

            order = await contract.orders(0)
            expect(
                order[0]
            ).to.be.eq(0);
            
            // 0.15 / 0.0000143 = 10 489 (solidity division)
            expect(
                await token.balanceOf(contract.address)
            ).to.be.eq(10489);

            await contract.connect(user_2).buyACDM(10489, {
                value: ethers.utils.parseEther("0.1499927")
            });

            await ethers.provider.send("evm_increaseTime", [300000])
            await contract.startTradeRound();

            await contract.connect(user_2).addOrder(
                10000,
                ethers.utils.parseEther("0.2")
            );

            order = await contract.orders(0);
            expect(
                order[0]
            ).to.be.eq(10000);
            expect(
                order[1]
            ).to.be.eq(ethers.utils.parseEther("0.2"));
            
            await contract.connect(user_1).redeemOrder(
                10000,
                0,
                {
                    value: ethers.utils.parseEther("0.2")
                }
            )

            order = await contract.orders(0);
            expect(
                order[0]
            ).to.be.eq(0);
            expect(
                order[1]
            ).to.be.eq(ethers.utils.parseEther("0"));

            await ethers.provider.send("evm_increaseTime", [300000])
            await contract.startSaleRound();
            
            // 0.2 / 0.000018729 = 10 678
            expect(
                await token.balanceOf(contract.address)
            ).to.be.eq(10678);
            
            const ref1BalanceBefore = await user_1.getBalance();
            const ref2BalanceBefore = await signers[0].getBalance();
            const balanceBefore = await user_2.getBalance();
            const sentTx = await contract.connect(user_2).withdraw();
            const minedTx = await sentTx.wait();
            const fee = minedTx.effectiveGasPrice.mul(minedTx.cumulativeGasUsed);
            const balanceAfter = await user_2.getBalance();
            const ref2BalanceAfter = await signers[0].getBalance();
            const ref1BalanceAfter = await user_1.getBalance();
            
            const expected= ethers.utils.parseEther("0.648493284");

            expect(
                balanceAfter.sub(balanceBefore)
            ).to.be.eq(expected.sub(fee));
            expect(
                ref1BalanceAfter.sub(ref1BalanceBefore)
            ).to.be.eq(ethers.utils.parseEther("0.031249635"));
            expect(
                ref2BalanceAfter.sub(ref2BalanceBefore)
            ).to.be.eq(ethers.utils.parseEther("0.020249781"));

            
            const ref1BalanceBefore1 = await signers[0].getBalance();
            const balanceBefore1 = await user_1.getBalance();
            const sentTx1 = await contract.connect(user_1).withdraw();
            const minedTx1 = await sentTx1.wait();
            const fee1 = minedTx1.effectiveGasPrice.mul(minedTx1.cumulativeGasUsed);
            const balanceAfter1 = await user_1.getBalance();
            const ref1BalanceAfter1 = await signers[0].getBalance();

            const expected1 = ethers.utils.parseEther("0.65")

            expect(
                balanceAfter1.sub(balanceBefore1)
            ).to.be.eq(expected1.sub(fee1));
            expect(
                ref1BalanceAfter1.sub(ref1BalanceBefore1)
            ).to.be.eq(ethers.utils.parseEther("0.03"));
        });
    });
});
