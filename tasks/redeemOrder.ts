import { task } from "hardhat/config";


task("redeemOrder", "Redeem order in TRADE round")
    .addParam("amountkzt", "amount of tokens")
    .addParam("id", "order id")
    .addParam("contract", "address of platform")
    .setAction(async (taskArgs, hre) => {
        const contract = await hre.ethers.getContractAt("ACDMPlatform", taskArgs.contract);
        await contract.redeemOrder(
            taskArgs.amountkzt,
            taskArgs.id
        );
    });