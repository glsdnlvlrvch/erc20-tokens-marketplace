//SPDX-License-Identifier: Unlicense
pragma solidity ^0.8.0;

import "hardhat/console.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "./Token.sol";

contract ACDMPlatform is ReentrancyGuard {
    enum STEP { SALE, TRADE }
    struct User {
        address refer_1;
        address refer_2;
        uint256 saleETH;
        uint256 tradeETH;
    }
    struct Order {
        uint256 amountACDM;
        uint256 amountETH;
        address owner;
    }

    address private ACDMToken;
    uint256 public roundTime; 
    uint256 public price;
    uint256 private volume;
    uint256 private _ordersId;
    uint256 private startTime;
    STEP private currentState;

    mapping(address => User) public users;
    mapping(uint256 => Order) public orders;

    event SaleRound(
        uint256 price,
        uint256 time,
        uint256 volume
    );

    modifier StepOnly(
        STEP state
    ) {
        require(
            currentState == state,
            "no permission in current round"
        );
        _;
    }

    constructor(
        address _ACDMToken,
        uint256 _roundTime
    ) {
        ACDMToken = _ACDMToken;
        roundTime = _roundTime * 1 days;
        currentState = STEP.SALE;
        price = 10000 gwei;
        startTime = block.timestamp;

        Token(ACDMToken).mint(
            address(this),
            100000
        );
    }

    function register(
        address refer
    ) external {
        User storage ref = users[refer];

        if(ref.refer_1 != address(0)){
            users[msg.sender].refer_2 = ref.refer_1;
        }

        users[msg.sender].refer_1 = refer;
    }

    function startSaleRound() external StepOnly(STEP.TRADE) {
        require(
            block.timestamp - startTime > roundTime,
            "round time already going"
        );

        _getBackTokens();

        _ordersId = 0;
        price = price * 103 / 100 + 4000 gwei;
        currentState = STEP.SALE;
        startTime = block.timestamp;

        uint256 saleVolume = volume / price;
        uint256 currBalance = IERC20(ACDMToken).balanceOf(address(this));

        if(saleVolume > currBalance){
            Token(ACDMToken).mint(
                address(this),
                saleVolume - currBalance
            );
        }
        else{
            Token(ACDMToken).burn(
                currBalance - saleVolume
            );
        }

        emit SaleRound(
            price,
            block.timestamp,
            saleVolume
        );
    }
 
    function startTradeRound() public StepOnly(STEP.SALE) {
        require(
            block.timestamp - startTime > roundTime,
            "round time already going"
        );

        currentState = STEP.TRADE;
        volume = 0;
        startTime = block.timestamp;
    }

    function buyACDM(
        uint256 amountACDM
    ) external payable nonReentrant StepOnly(STEP.SALE) {
        require(
            _enoughBalance(amountACDM),
            "not enough balance"
        );

        uint256 amountETH = price * amountACDM;
        require(
            msg.value == amountETH,
            "invalid value"
        );

        IERC20(ACDMToken).transfer(
            msg.sender,
            amountACDM
        );

        User storage user = users[msg.sender];
        user.saleETH += amountETH;
    }

    function addOrder(
        uint256 amountACDM,
        uint256 amountETH
    ) external StepOnly(STEP.TRADE) {
        IERC20(ACDMToken).transferFrom(
            msg.sender,
            address(this),
            amountACDM
        );

        Order storage ord = orders[_ordersId];
        ord.amountACDM = amountACDM;
        ord.amountETH = amountETH;
        ord.owner = msg.sender;

        _ordersId++;
    }

    function removeOrder(
        uint256 idOrder
    ) external nonReentrant StepOnly(STEP.TRADE) {
        Order storage ord = orders[idOrder];
        require(
            ord.amountETH != 0,
            "invalid order id"
        );
        require(
            msg.sender == ord.owner,
            "you have no rights"
        );

        IERC20(ACDMToken).transfer(
            ord.owner,
            ord.amountACDM
        );

        delete orders[idOrder];
    }

    function redeemOrder(
        uint256 amountACDM, 
        uint256 idOrder
    ) external payable nonReentrant StepOnly(STEP.TRADE) {
        Order storage ord = orders[idOrder];
        require(
            ord.owner != address(0),
            "invalid order id"
        );
        require(
            ord.amountACDM >= amountACDM,
            "too much ACDM"
        );

        uint256 amountETH = amountACDM * (ord.amountETH / ord.amountACDM);
        require(
            msg.value == amountETH,
            "invalid ETH amount"
        );

        IERC20(ACDMToken).transfer(
            msg.sender,
            amountACDM
        );

        volume += amountETH;
        ord.amountACDM -= amountACDM;
        ord.amountETH -= amountETH;

        if(ord.amountACDM == 0){
            delete orders[idOrder];
        }

        User storage user = users[msg.sender];
        user.tradeETH += amountETH;
    }

    function withdraw() external nonReentrant {
        User storage user = users[msg.sender];

        if(user.saleETH > 0) _payInSaleETH(msg.sender);
        if(user.tradeETH > 0) _payInTradeETH(msg.sender);
    }

    function _getBackTokens() internal {
        for(uint i = 0; i < _ordersId; i++){
            Order storage order = orders[i];

            if(order.owner != address(0)){
                IERC20(ACDMToken).transfer(
                    order.owner,
                    order.amountACDM
                );
                delete orders[i];
            }
        }
    }

    function _enoughBalance(
        uint256 amountACDM
    ) internal returns (bool) {
        if(amountACDM > IERC20(ACDMToken).balanceOf(address(this))) {
            return false;
        }
        return true;
    }

    function _payInSaleETH(
        address recipient
    ) internal {
        User storage user = users[recipient];

        uint256 refAmount1 = user.saleETH * 5 / 100;
        uint256 refAmount2 = user.saleETH * 3 / 100;

        if(user.refer_1 != address(0)) {
            (bool success, ) = user.refer_1.call{value: refAmount1}("");
            require(
                success,
                "ERROR"
            );
        }

        if(user.refer_2 != address(0)) {
            (bool success, ) = user.refer_2.call{value: refAmount2}("");
            require(
                success,
                "ERROR"
            );
        }

        (bool success, ) = recipient.call{value: user.saleETH - (refAmount1 + refAmount2)}("");
        require(
            success,
            "ERROR"
        );

        user.saleETH = 0;
    }

    function _payInTradeETH(
        address recipient
    ) internal {
        User storage user = users[recipient];
        uint256 refAmount = user.tradeETH * 25 / 1000;

        if(user.refer_1 != address(0)) {
            (bool success, ) = user.refer_1.call{value: refAmount}("");
            require(
                success,
                "ERROR"
            );
        }

        if(user.refer_1 != address(0)) {
            (bool success, ) = user.refer_2.call{value: refAmount}("");
            require(
                success,
                "ERROR"
            );
        }

        (bool success, ) = recipient.call{value: user.tradeETH - (refAmount + refAmount)}("");
        require(
            success,
            "ERROR"
        );

        user.tradeETH = 0;
    } 
}
