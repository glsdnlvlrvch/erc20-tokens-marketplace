import { task } from "hardhat/config";


task("withdraw", "Withdraw ETH from saling and trading")
    .addParam("contract", "address of platform")
    .setAction(async (taskArgs, hre) => {
        const contract = await hre.ethers.getContractAt("ACDMPlatform", taskArgs.contract);
        await contract.withdraw(
        );
    });