import { task } from "hardhat/config";


task("buy", "Buy tokens in SALE round")
    .addParam("amount", "amount of tokens")
    .addParam("contract", "address of platform")
    .setAction(async (taskArgs, hre) => {
        const contract = await hre.ethers.getContractAt("ACDMPlatform", taskArgs.contract);
        await contract.buyACDM(
            taskArgs.amount,
        );
    });